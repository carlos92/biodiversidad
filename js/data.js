var animales = [
	{
		"nombre" : "aullador negro",
		"shape" : "mono.png",
		"image":"53676_orig.jpg",
		"descripcion": "Cuerpo de color negro a excepción de un fleco que corre lateralmente desde el brazo hasta la ingle de color pálido, amarillo o café pálido. Garganta prominente, rostro desnudo y de color negro.",
		"url": "http://www.biodiversidad.co/fichas/112",
		"region": "Pacifico",
		"x": 472,
		"y": 226
	},
	{
		"nombre" : "Sapo",
		"shape" : "sapo.png",
		"image":"10364_orig.jpg",
		"descripcion": "Generalmente su coloración en el dorso es marrón y la piel es suave, mientras que el vientre es color marrón grisáceo y de textura granular. Presenta manchas negras irregulares.",
		"url": "http://www.biodiversidad.co/fichas/3950",
		"region": "Amazonas",
		"x": 472,
		"y": 226
	},
	{
		"nombre" : "amazilla pechiazul – colibri",
		"shape" : "colibri.png",
		"image":"45036_orig.jpg",
		"descripcion": "A. amabilis tiene en promedio un largo total de 8.9 cm, pico recto (18mm), mandíbula inferior basalmente rosa opaco. Macho con coronilla verde iridiscente; el resto es verde brillante por encima; alta garganta verde moreno; centro de baja garganta y pecho con parche de azul violeta iridiscente.",
		"url": "http://www.biodiversidad.co/fichas/2541",
		"region": "Andina",
		"x": 472,
		"y": 226
	},
	{
		"nombre" : "escarabajo copografo - anisocanthon villosus",
		"shape" : "escarabajo.png",
		"image":"27468_orig.jpg",
		"descripcion": "Cabeza con el margen anterior del clípeo en arco regular en los lados de los dientes medios; superficie muy débilmente puntuada. Pronoto con los ángulos anteriores casi rectos, salientes, las márgenes laterales con el ángulo bien evidente; superficie irregular, con depresiones en los lados.",
		"url": "http://www.biodiversidad.co/fichas/3373",
		"region": "Orinoquia",
		"x": 472,
		"y": 226
	},
	{
		"nombre" : "guacamaya roja",
		"shape" : "guacamaya.png",
		"image":"BIR-0735A_Ara_chloropterus_270x270.jpg",
		"descripcion": "Guacamaya de gran tamaño (84 a 96 cm) y coloración roja, similar al Guacamayo escarlata (Ara macao) del cual se diferencia por el rojo más oscuro, rostro decorado por delgadas líneas de plumas rojas",
		"url": "http://www.biodiversidad.co/fichas/3814",
		"region": "Caribe",
		"x": 472,
		"y": 226
	},
	{
		"nombre" : "tucan azul",
		"shape" : "tucan.png",
		"image":"25160_orig.jpg",
		"descripcion": "Se alimentan en pares o grupos familiares, principalmente en el dosel o en el estrato medio del bosque no suele ser muy alto (Handbook of the birds of the world, Hilty & Brown 1986 , Renjifo et al. 2002).",
		"url": "http://www.biodiversidad.co/fichas/34",
		"region": "Andina",
		"x": 501,
		"y": 400
	},
	{
		"nombre" : "perro de monte",
		"shape" : "perro_del_monte.png",
		"image":"01287_orig.jpg",
		"descripcion": "El perro de orejas cortas se encuentra en bosques no perturbados. Se ha registrado en una variedad de hábitats de tierras bajas, incluyendo bosques de tierra firme, bosques húmedos y bosques en sucesión primaria a lo largo de los ríos.",
		"url": "http://www.biodiversidad.co/fichas/420",
		"region": "Amazonas",
		"x": 501,
		"y": 363
	},
	{
		"nombre" : "hormiga culona",
		"shape" : "hormiga_culona.png",
		"image":"70393_orig.jpg",
		"descripcion": "Son hormigas cortadoras de hojas. Estas son utilizadas para sembrar el hongo simbiótico Rhozites gongylophora. En la dieta de la obrera, la savia absorbida en el momento del corte representa el 95 % de su alimentación y el 5% restante es proporcionado por el hongo.",
		"url": "http://www.biodiversidad.co/fichas/277",
		"region": "Andina",
		"x": 600,
		"y": 167
	},
	{
		"nombre" : "caiman de frente lisa",
		"shape" : "caiman.png",
		"image":"17112_orig.jpg",
		"descripcion": "Los individuos recién nacidos y los juveniles se alimentan principalmente de invertebrados como crustáceos y otros invertebrados terrestres, en tanto que los adultos consumen mayor cantidad de peces (Rueda-Almonacid et al. 2007). Romão et al. (2011) caracterizan anatómica y topográficamente el sistema digestivo de este reptil.",
		"url": "http://www.biodiversidad.co/fichas/3992",
		"region": "Orinoquia",
		"x": 600,
		"y": 300
	},
	{
		"nombre" : "oso perezoso",
		"shape" : "oso_perezoso.png",
		"image":"04375_orig.jpg",
		"descripcion": "El oso perezoso es un animal nocturno arborícola y solitario (Emmons y Feer 1997). ). Solamente desciende de los árboles al suelo para defecar (Morales-Jiménez 2004).",
		"url": "http://www.biodiversidad.co/fichas/662",
		"region": "Caribe",
		"x": 377,
		"y": 300
	},
	{
		"nombre" : "delfin rosado",
		"shape" : "delfin_rosado.png",
		"image":"48978_orig.jpg",
		"descripcion": "Inia geoffrensis se caracteriza por tener el cuerpo robusto y flexible, con un hocico largo y estrecho. Los machos son ligeramente más grandes que las hembras con longitudes hasta de 2,78 m. y pesos alrededor de 180 kg. ",
		"url": "http://www.biodiversidad.co/fichas/378",
		"region": "Amazonas",
		"x": 567,
		"y": 461
	},
	{
		"nombre" : "mico de noche",
		"shape" : "mico_de_noche.png",
		"image":"80411_orig.jpg",
		"descripcion": "Dorso pardo, gris o café. Pelaje denso y lanoso. Vientre amarillo pálido a naranja brillante. Cabeza con pelaje corto. Cara café bordeada de blanco. Frente con tres franjas negras y una mancha larga central entre los ojos.",
		"url": "http://www.biodiversidad.co/fichas/115",
		"region": "Andina",
		"x": 570,
		"y": 248
	},
	{
		"nombre" : "sardina",
		"shape" : "sardina.png",
		"image":"250px-Sardina_pilchardus_2011.jpg",
		"descripcion": "Se encuentra en ríos de gran cauce en ambientes donde predominan los sitios protegidos de las fuertes corrientes y con acumulación de palizadas o vegetación sumergida, típicos de las zonas de inundación y cerca de las desembocaduras de pequeños subsidiarios. ",
		"url": "http://www.biodiversidad.co/fichas/683",
		"region": "Andina",
		"x": 480,
		"y": 300
	},
	{
		"nombre" : "murcielago vigotudo",
		"shape" : "murcialago.png",
		"image":"73542_orig.jpg",
		"descripcion": "Tiene el hocico corto y ancho; la placa labio-nasal simple; el margen superior de los nostrilos tiene algunas protuberancias en forma de verrugas; hay una emarginación profunda entre los nostrilos, con una protuberancia carnosa en el dorso.",
		"url": "http://www.biodiversidad.co/fichas/4149",
		"region": "Caribe",
		"x": 636,
		"y": 125
	},
	{
		"nombre" : "mojarra negra",
		"shape" : "mojarra_negra.png",
		"image":"83647_orig.jpg",
		"descripcion": "La coloración cambia según la edad y el estado del animal: los pequeños tienen grandes manchas amarillo doradas, sobre un fondo marrón oliváceo, los adultos tienden a volverse más oscuros porque las manchas son más pequeñas. ",
		"url": "http://www.biodiversidad.co/fichas/573",
		"region": "Amazonas",
		"x": 596,
		"y": 408
	},
	{
		"nombre" : "braceador – mono",
		"shape" : "braceador.png",
		"image":"93404_orig.jpg",
		"descripcion": "Se encuentran en bosques húmedos hasta 1300 metros de altitud. Viven en bosques maduros e intervenidos. Se encuentran generalmente en el dosel. Son diurnos y arborícolas. Viven solitarios o en grupos de hasta 20 individuos.",
		"url": "http://www.biodiversidad.co/fichas/116",
		"region": "Orinoquia",
		"x": 377,
		"y": 280
	},
	{
		"nombre" : "gabilan pescador",
		"shape" : "gabilan_pescador.png",
		"image":"50880_orig.jpg",
		"descripcion": "El gavilán pescador es una especie tropical que se encuentra asociada a cuerpos de agua de sitios abiertos, en donde puede llegar a ser común. También puede encontrarse a lo largo de diques y reservorios artificiales en donde utiliza postes de cercas como perchas (Brown y Amadon 1968).",
		"url": "http://www.biodiversidad.co/fichas/174",
		"region": "Orinoquia",
		"x": 433,
		"y": 148
	},
	{
		"nombre" : "caiman comun",
		"shape" : "caiman_comun.png",
		"image":"Caiman_crocodilus_270x270.jpg",
		"descripcion": "Esta es una especie de amplia adaptabilidad a muy variados hábitats acuáticos de las tierras bajas, donde se encuentra tanto en climas muy secos como muy húmedos. Esta especie es de tamaño relativamente pequeño, los machos alcanzan excepcionalmente 2, 70 m de longitud y las hembras no más 1, 80 m.",
		"url": "http://www.biodiversidad.co/fichas/174",
		"region": "Andina",
		"x": 425,
		"y": 350
	},
	{
		"nombre" : "chucha rata",
		"shape" : "chucha_rata.png",
		"image":"Caluromys_lanatus_270x270.jpg",
		"descripcion": "Esta especie habita en bosques húmedos y de galería. Se encuentran desde plantaciones y jardines hasta bosques maduros (Morales-Jiménez et al. 2004). Los individuos de Caluromys lanatus se alimentan de flores y probablemente de algunos invertebrados, huevos, polluelos y otros vertebrados (Morales-Jiménez et al. 2004)",
		"url": "http://www.biodiversidad.co/fichas/122",
		"region": "Pacifico",
		"x": 547,
		"y": 280
	},
	{
		"nombre" : "zorro sabanero",
		"shape" : "zorro_sabanero.png",
		"image":"Cerdocyon_thous_270x270.jpg",
		"descripcion": "El perro de monte busca sus presas principalmente en la noche, caza invididualmente, pero también en parejas y puede estar acompañado por 1 a 3 crías. La caza cooperativa es rara.",
		"url": "http://www.biodiversidad.co/fichas/421",
		"region": "Orinoquia",
		"x": 385,
		"y": 223
	},
	{
		"nombre" : "comadreja de agua",
		"shape" : "comadreja_agua.png",
		"image":"zorrodeagua2.jpg",
		"descripcion": "Esta especie se limita a las zonas de agua permanente, como arroyos o ríos, por lo general dentro de un bosque. Es un excelente nadador y buceador, remando con sus patas traseras y usando su cola como un timón.",
		"url": "http://www.biodiversidad.co/fichas/3489",
		"region": "Andina",
		"x": 440,
		"y": 430
	},
	{
		"nombre" : "rana",
		"shape" : "rana.png",
		"image":"55551_orig.jpg",
		"descripcion": "Es una especie terrestre y fosorial, usualmente se observa en el suelo de bosques primarios durante la noche (Rodríguez y Duellman. 1994). Fácilmente reconocible por las bandas negras en la ingle y el dorso y flancos café con una línea pos orbital crema.",
		"url": "http://www.biodiversidad.co/fichas/2533",
		"region": "Orinoquia",
		"x": 440,
		"y": 367
	},
	{
		"nombre" : "lobo de crin",
		"shape" : "lobo_crin.png",
		"image":"52263_orig.jpg",
		"descripcion": "Las parejas de lobos de crin ocupan territorios aproximadamente de 30 km2. Son principalmente nocturnos o crepusculares en sus patrones de actividad (Sheldon 1992). Las parejas comparten un territorio pero raramente son vistos juntos (Fletchall et al. 2000).",
		"url": "http://www.biodiversidad.co/fichas/422",
		"region": "Orinoquia",
		"x": 517,
		"y": 300
	},
	{
		"nombre" : "tortola azul",
		"shape" : "tortola_azul.png",
		"image":"20454_orig.jpg",
		"descripcion": "La Tortolita azul acostumbra a estar en pareja o con menos frecuencia en grupos pequeños, más bien arisca. Se alimenta en el suelo, claros sombreados, y orillas de carretera pero vista con menos frecuencia. Cuando vuela lo hace mas alto que las otras tortolitas (Hilty & Brown 2001).",
		"url": "http://www.biodiversidad.co/fichas/2515",
		"region": "Pacifico",
		"x": 536,
		"y": 188
	},
	{
		"nombre" : "cachama negra",
		"shape" : "cachama_negra.png",
		"image":"98024_orig.jpg",
		"descripcion": "Pez da hábitos grupales en sus desplazamientos tanto de juvenil como adulto (González, 2001 en Mojica et al 2002: 194), se alimentan formando agrupaciones debajo de los árboles frutales (Damaso, J., 2006:25)",
		"url": "http://www.biodiversidad.co/fichas/587",
		"region": "Orinoquia",
		"x": 686,
		"y": 188
	},
	{
		"nombre" : "armadillo",
		"shape" : "armadillo.png",
		"image":"19534_orig.jpg",
		"descripcion": "Su dieta típica esta constituida en un 75% por artrópodos entre los cuales están los insectos (Coleóptera, Hymenóptera, Lepidóptera, Orthóptera, Isóptera, Hemíptera y Díptera), arácnidos, miriápodos y crustaceos; 15 % de material animal (incluidos anfibios, reptiles, mamíferos, aves y sus huevos) y un 10% de vegetales.",
		"url": "http://www.biodiversidad.co/fichas/275",
		"region": "Amazonas",
		"x": 561,
		"y": 119
	},
	{
		"nombre" : "rana payaso",
		"shape" : "rana_payaso.png",
		"image":"Dendropsophus_bifurcus2_Santiago_Ron_270x270.jpg",
		"descripcion": "Habita arbustos en bosques abiertos y claros, con frecuencia se pueden encontrar en áreas perturbadas (Ron 2011). Se alimenta de escarabajos de la familia Ptilodactylidae (Menéndez 2001).",
		"url": "http://www.biodiversidad.co/fichas/3864",
		"region": "Amazonas",
		"x": 316,
		"y": 237
	},
	{
		"nombre" : "comadreja grande",
		"shape" : "comadreja_grande.png",
		"image":"25044_orig.jpg",
		"descripcion": "Esta especie tiene una longitud total de 1000 mm, de los cuales 482 mm son de la cabeza y el cuerpo y 535 mm son de la cola. Las hembras son generalmente más pequeñas que los machos.",
		"url": "http://www.biodiversidad.co/fichas/2509",
		"region": "Pacifico",
		"x": 503,
		"y": 110
	},
	{
		"nombre" : "garzon azul",
		"shape" : "garzon_azul.png",
		"image":"03929_orig.jpg",
		"descripcion": "Esta ave es generalmente solitaria y arisca especialmente durante la época de anidación. Su vuelo es algo lento y laborioso (Hilty y Brown 1986). La dieta alimenticia del garzón azul, esta constituida principalmente por peces y anfibios (Olivares A. 1973).",
		"url": "http://www.biodiversidad.co/fichas/414",
		"region": "Caribe",
		"x": 332,
		"y": 422
	},
	{
		"nombre" : "tucan verde",
		"shape" : "tucan_verde.png",
		"image":"76104_orig.jpg",
		"descripcion": "Las visitas normales a un árbol para alimentarse duran entre 2 y 8 minutos, se han registrado muy pocos casos hasta de 4 horas. Consumen en promedio 26 g de fruta por visita y la digestión dura de 24 a 70 minutos, así que, en una visita, pueden consumir una mayor cantidad de frutos pequeños que de grandes. ",
		"url": "http://www.biodiversidad.co/fichas/37",
		"region": "Andina",
		"x": 501,
		"y": 172
	},
	{
		"nombre" : "pato real",
		"shape" : "pato_real.png",
		"image":"Cairina_Moschata_macho_hembra_270x270.jpg",
		"descripcion": "El pato real suele encontrarse áreas cercanas a bosques, pantanos, lagunas de agua dulce, cerca de áreas boscosas y manglares. Actualmente se encuentra exterminado de gran parte de los valles del Magdalena y Cauca, en otras regiones es escaso y perseguido por cazadores (Hilty y Brown 1986).",
		"url": "http://www.biodiversidad.co/fichas/415",
		"region": "Caribe",
		"x": 462,
		"y": 356
	},
	{
		"nombre" : "tigrillo",
		"shape" : "tigrillo.png",
		"image":"41276_orig.jpg",
		"descripcion": "Leopardus pardalis ocupa una gran variedad de hábitats, desde las selvas tropicales húmedas, matorrales secos y espinosos, sabanas, ciénagas y pantanos (Nowell y Jackson 1996, Eisenberg 1989). ",
		"url": "http://www.biodiversidad.co/fichas/339",
		"region": "Andina",
		"x": 498,
		"y": 339
	},
	{
		"nombre" : "lobito de rio",
		"shape" : "lobillo_de_rio.png",
		"image":"35038_orig.jpg",
		"descripcion": "La nutria es más tímida que el perro de agua (Pteronura brasiliensis) y a pesar de tener hábitos diurnos (Husson 1978), presenta también actividad crepuscular y nocturna. La frecuencia de encuentro de esta especie en el Cañón del río Alicante (Antioquia) es de 0,78 ind/km (Arcila obs. per.)",
		"url": "http://www.biodiversidad.co/fichas/339",
		"region": "Andina",
		"x": 411,
		"y": 219
	},
	{
		"nombre" : "marmosa raton",
		"shape" : "marmosa_raton.png",
		"image":"38363_orig.jpg",
		"descripcion": "Esta especie en promedio mide 332 mm (250- 358), la cabeza y el cuerpo mide de 110 a 146 mm; la cola es tan larga como la cabeza y el cuerpo (110-212mm). M. murina>/i> tiene un pelaje dorsal de color de ante pálido que contrasta con la parte crema de partes posteriores",
		"url": "http://www.biodiversidad.co/fichas/2459",
		"region": "Andina",
		"x": 526,
		"y": 221
	},
	{
		"nombre" : "tigre real",
		"shape" : "tigre_real.png",
		"image":"Panthera_onca(1)_270x270.jpg",
		"descripcion": "El jaguar y el puma (Puma concolor) sobreponen sus áreas de distribución, pero dividen dicho territorio, por lo que se asume que las dos especies difieren ecológicamente y de esta manera pueden coexistir de manera estable.",
		"url": "http://www.biodiversidad.co/fichas/338",
		"region": "Amazonas",
		"x": 526,
		"y": 186
	},
	{
		"nombre" : "ocarro – armadillo gigante",
		"shape" : "armadillo_gigante.png",
		"image":"Priodontes_maximus_270x270.jpg",
		"descripcion": "Priodontes maximus se encuentra en bosques no intervenidos cercanos a cursos de agua, sin embargo, un estudio realizado en Brasil reportó un 68% de las madrigueras en pastizales 28% en arbustales y solo un 3% en áreas boscosas, con más de la mitad de las madrigueras en termiteros activos (Nowak 1999).",
		"url": "http://www.biodiversidad.co/fichas/342",
		"region": "Amazonas",
		"x": 627,
		"y": 186
	},
	{
		"nombre" : "puma",
		"shape" : "pantera.png",
		"image":"88950_orig.jpg",
		"descripcion": "El puma tolera una amplia gama de hábitats, incluyendo bosques húmedos, bosque seco, sabana, humedales, llanos y desiertos, incluso suele ocupar el páramo y bosque andino o montano hasta los 5800 m. de altura, en zonas de Bolivia, Chile y Argentina (Redford y Eisenberg 1992). ",
		"url": "http://www.biodiversidad.co/fichas/348",
		"region": "Amazonas",
		"x": 230,
		"y": 280
	},
	{
		"nombre" : "tapir terrestre",
		"shape" : "tapir.png",
		"image":"21984_orig.jpg",
		"descripcion": "Los principales depredadores de la danta son el jaguar (Pantera onca), el puma (Puma concolor) y cocodrilos grandes (Padilla y Dowler 1994). La danta común es una especie que presenta densidades poblacionales bajas al ser comparada con otros ungulados amazónicos: 4,0 individuos/ 10 km2 en la Amazonia peruana (Bodmer 1997) y 2,8 individuos / 100 km2 en la Amazonia norte de Colombia (Solano y Vargas 1999).",
		"url": "http://www.biodiversidad.co/fichas/340",
		"region": "Orinoquia",
		"x": 593,
		"y": 230
	},
	{
		"nombre" : "manati del caribe",
		"shape" : "manati.png",
		"image":"25175_orig.jpg",
		"descripcion": "Los principales depredadores de la danta son el jaguar (Pantera onca), el puma (Puma concolor) y cocodrilos grandes (Padilla y Dowler 1994). La danta común es una especie que presenta densidades poblacionales bajas al ser comparada con otros ungulados amazónicos: 4,0 individuos/ 10 km2 en la Amazonia peruana (Bodmer 1997) y 2,8 individuos / 100 km2 en la Amazonia norte de Colombia (Solano y Vargas 1999).",
		"url": "http://www.biodiversidad.co/fichas/295",
		"region": "Caribe",
		"x": 398,
		"y": 230
	},
	{
		"nombre" : "oso andino",
		"shape" : "oso_andino.png",
		"image":"08502_orig.jpg",
		"descripcion": "La importancia ecológica del oso de anteojos radica en su posible papel como dispersor de semillas, aunque también puede ser un importante trasformador del bosque al derribar arbustos y ramas para alimentarse (Andrade 2001, citado en Rodríguez et al., 1986) . ",
		"url": "http://www.biodiversidad.co/fichas/281",
		"region": "Andina",
		"x": 398,
		"y": 464
	},
	{
		"nombre" : "muricielago frutero",
		"shape" : "murcialago_frutero.png",
		"image":"16257_orig.jpg",
		"descripcion": "Esta especie es un importante dispersor de semillas de yarumos, guayabos y piperáceas, entre muchas otras, por esto juega un papel fundamental del equilibrio ecológico (Muñoz Arango 2001).",
		"url": "http://www.biodiversidad.co/fichas/2452",
		"region": "Caribe",
		"x": 550,
		"y": 187
	},
	{
		"nombre" : "puerco espin",
		"shape" : "puerco_espin.png",
		"image":"63413_orig.jpg",
		"descripcion": "La parte dorsal de Coendou prehensilis está cubierta por espinas fuertes con púas retrorsas, muchas espinas son tricolor con bandas casi igual de ancho, blancas o amarillo pálido en la base y la punta negras o marrón oscuro en el centro; el color general es negro o marrón oscuro densamente cubierto con blanco y amarillento; sin pelo suave entre las espinas; las espinas de la cabeza, patas y cola son cortas; las espinas de la espalda son largas y gruesas",
		"url": "http://www.biodiversidad.co/fichas/2456",
		"region": "Amazonas",
		"x": 393,
		"y": 187
	},
	{
		"nombre" : "puerco espin",
		"shape" : "mapache.png",
		"image":"43957_orig.jpg",
		"descripcion": "Está especie es nocturna, terrestre y solitaria. Tiende a forrajear solo excepto por la hembras con descendencia (Eisenberg 1989). La dieta de esta especie consiste en moluscos, peces, cangrejos, insectos y anfibios (Emmons 1990). Está especie frecuentemente se encuentra asociada a corrientes, lagunas o lagos (Eisenberg 1989).",
		"url": "http://www.biodiversidad.co/fichas/2469",
		"region": "Caribe",
		"x": 393,
		"y": 400
	},
	{
		"nombre" : "gato colorado",
		"shape" : "gato_colorado.png",
		"image":"64682_orig.jpg",
		"descripcion": "La dieta de Puma yagouaroundi incluye pequeños mamíferos, pájaros y reptiles",
		"url": "http://www.biodiversidad.co/fichas/2471",
		"region": "Andina",
		"x": 624,
		"y": 216
	},
	{
		"nombre" : "oso hormiguero comun",
		"shape" : "oso_hormiguero.png",
		"image":"37125_orig.jpg",
		"descripcion": "Esta especie posee cabeza y patas color oro, y en el vientre y parte inferior de la espalda tiene color negro a estilo de chaleco. Su pelaje es duro, cabeza alargada y angosta, el hocico es pelado hasta los ojos negruzco, cola larga y peluda con excepcion de la punta, se distingue de los tamanduas del norte por caracteristicas craneanas.",
		"url": "http://www.biodiversidad.co/fichas/2493",
		"region": "Andina",
		"x": 609,
		"y": 290
	},
	{
		"nombre" : "tortuga arrau",
		"shape" : "tortuga_arau.png",
		"image":"37769_orig.jpg",
		"descripcion": "Está especie vive en el agua, salvo cuando tiene que salir a las playas a poner huevos. Sin embargo pueden caminar en tierra firme, pero de manera lenta y poco ágil (von Hildebrand 1999).",
		"url": "http://www.biodiversidad.co/fichas/301",
		"region": "Orinoquia",
		"x": 666,
		"y": 240
	},
	{
		"nombre" : "tortuga de rio",
		"shape" : "tortuga_de_rio.png",
		"image":"Podocnemis_lewyana_270x270.jpg",
		"descripcion": "Podocnemis lewyana es una especie herbívora al menos en su estado adulto (Hurtado-Sepúlveda 1973). Podocnemis lewyana se encuentra en remansos de los ríos grandes y pequeños y en ciénagas grandes (Castaño-Mora y Medem 2002).",
		"url": "http://www.biodiversidad.co/fichas/680",
		"region": "Caribe",
		"x": 508,
		"y": 299
	},
	{
		"nombre" : "serpiente de tierra",
		"shape" : "serpiente_de_tierra.png",
		"image":"44527_orig.jpg",
		"descripcion": "Bosque muy húmedo premontano, bosque seco húmedo y muy húmedo montano bajo, bosque muy húmedo montano. Esta es una especie minadora y es usual encontrarla bajo troncos y rocas, también asociada a cuerpos de agua.",
		"url": "http://www.biodiversidad.co/fichas/2792",
		"region": "Andina",
		"x": 546,
		"y": 317
	},
	{
		"nombre" : "boa",
		"shape" : "boa.png",
		"image":"08620_orig.jpg",
		"descripcion": "Bosque muy húmedo premontano, bosque seco húmedo y muy húmedo montano bajo, bosque muy húmedo montano. Esta es una especie minadora y es usual encontrarla bajo troncos y rocas, también asociada a cuerpos de agua.",
		"url": "http://www.biodiversidad.co/fichas/2789",
		"region": "Amazonas",
		"x": 546,
		"y": 317
	},
	{
		"nombre" : "cazadora",
		"shape" : "cazadora.png",
		"image":"2799806904_213421302b.jpg",
		"descripcion": "Bosque muy húmedo premontano, bosque seco húmedo y muy húmedo montano bajo, bosque muy húmedo montano. Esta es una especie minadora y es usual encontrarla bajo troncos y rocas, también asociada a cuerpos de agua.",
		"url": "http://www.biodiversidad.co/fichas/2795",
		"region": "Andina",
		"x": 460,
		"y": 454
	},
	{
		"nombre" : "cocodrilo del orinoco",
		"shape" : "cocodrilo_de_orinoco.png",
		"image":"98658_orig.jpg",
		"descripcion": "Esta especie posee un hocico alargado ya se trate de ejemplares juveniles o adultos. posee seis placas cervicales Con 68 dientes en su formula dentaria incluye cinco premaxilares, 14 maxilares y 15 mandibulares (Medem 198).",
		"url": "http://www.biodiversidad.co/fichas/300",
		"region": "Orinoquia",
		"x": 383,
		"y": 332
	},
	{
		"nombre" : "cascabel",
		"shape" : "cascabel.png",
		"image":"33476_orig.jpg",
		"descripcion": "La serpiente cascabel se alimenta principalmente de mamíferos pequeños como ratas, ratones, ardillas, y puede ocasionalmente incluir presas más grandes como tlacuaches (marsupiales de México) y a veces consume lagartijas (Almeida-Santos et al. 2000)",
		"url": "http://www.biodiversidad.co/fichas/467",
		"region": "Caribe",
		"x": 440,
		"y": 270
	},
	{
		"nombre" : "tortuga dolfina",
		"shape" : "tortufa_dolfina.png",
		"image":"69365_orig.jpg",
		"descripcion": "Se alimentan casi exclusivamente de crustáceos decapados como langostas y cangrejos, también incluyen en su dieta equinodermos moluscos, peces y algas (Ernst et al. 2000, Savage 2002).",
		"url": "http://www.biodiversidad.co/fichas/2754",
		"region": "Pacifico",
		"x": 486,
		"y": 363
	},
	{
		"nombre" : "camaleon",
		"shape" : "camaleon.png",
		"image":"65888_orig.jpg",
		"descripcion": "Esta especie es arborícola, se le puede encontrar en matorrales espinosos , en selvas húmedas tropicales, bosques deciduos, semideciduos, bosques secos y bosques montanos (Rueda-Almonacid et al 2008 en Rodríguez-Maecha).",
		"url": "http://www.biodiversidad.co/fichas/2828",
		"region": "Amazonas",
		"x": 440,
		"y": 307
	},
	{
		"nombre" : "iguana verde",
		"shape" : "iguana_verde.png",
		"image":"84940_orig.jpg",
		"descripcion": "El cuerpo de la iguana es robusto, con cuatro fuertes patas provistas de uñas duras y afiladas, la piel es seca, con escamas pequeñas y puntos como espinas alrededor del cuello, con una glándula bien desarrollada debajo del oído (membrana timpánica) y un gran pliegue gular en la garganta, con una cresta de púas en forma de peine.",
		"url": "http://www.biodiversidad.co/fichas/423",
		"region": "Pacifico",
		"x": 509,
		"y": 408
	},
	{
		"nombre" : "salamanqueja",
		"shape" : "salmanqueja.png",
		"image":"19054_orig.jpg",
		"descripcion": "Se encuentran principalmente en zonas secas, bosque seco y muy seco, bosque húmedo tropical y bosque húmedo premontano; están casi exclusivamente asociados a la actividad humana, por lo que se les puede encontrar usualmente en las paredes de las casas en las horas de la noche (Köhler 2003).",
		"url": "http://www.biodiversidad.co/fichas/2807",
		"region": "Caribe",
		"x": 523,
		"y": 225
	},
	{
		"nombre" : "libelula",
		"shape" : "libelulas.png",
		"image":"Proneura_prolongata_-_Cornelio_Bota_270x270.jpg",
		"descripcion": "Longitud del cuerpo (incluyendo el abdomen) 36 mm. Ala posterior de 14 mm de longitud. Paraproctos cónicos con las puntas curvadas hacia adentro, cuatro veces más largos que los cercos. Los cercos son 2.5 veces más anchos que largos con amplia base en vista medio-dorsal.",
		"url": "http://www.biodiversidad.co/fichas/3953",
		"region": "Amazonas",
		"x": 390,
		"y": 260
	},
	{
		"nombre" : "avispa",
		"shape" : "avispa.png",
		"image":"1397039746-1.jpg",
		"descripcion": "El nido se ubica en oquedades de troncos y puede tener 11 panales con unas 6500 avispas.",
		"url": "http://www.biodiversidad.co/fichas/2914",
		"region": "Amazonas",
		"x": 366,
		"y": 320
	},
	{
		"nombre" : "abejita amazonica",
		"shape" : "abeja.png",
		"image":"bee.jpg",
		"descripcion": "Son abejas grandes (23-25mm. de largo), su cuerpo es aplanado y de color oscuro. Son muy poco observadas o coleccionadas, en consecuencia se sabe muy poco sobre su biología.",
		"url": "http://www.biodiversidad.co/fichas/2485",
		"region": "Amazonas",
		"x": 421,
		"y": 386
	},
	{
		"nombre" : "escarabajo rinoceronte",
		"shape" : "escarabajo_rinoceronte.png",
		"image":"Copris_hispanus.jpg",
		"descripcion": "Los machos de esta especie se caracterizan por su color negro muy brillante y porque los cuernos pronotales son delgados apicalmente y bien divergentes. Los machos pueden alcanzar tallas de 12 cm.",
		"url": "http://www.biodiversidad.co/fichas/2856",
		"region": "Amazonas",
		"x": 421,
		"y": 386
	},
	{
		"nombre" : "aguila cuaresmera",
		"shape" : "aguila_cuaresmera.png",
		"image":"15981_orig.jpg",
		"descripcion": "Grande de alas anchas y largas, franja pectoral conspicua y cola con franjas angostas, puntas de las remiges oscuras (fase normal). Especie similares: Geranoaetus melanoleucus.",
		"url": "http://www.biodiversidad.co/fichas/183",
		"region": "Andina",
		"x": 567,
		"y": 131
	},
	{
		"nombre" : "halcon murcielagero",
		"shape" : "halcon_murcielago.png",
		"image":"50924_orig.jpg",
		"descripcion": "Pequeño, compacto, negro con garganta y cuello blanco o crema, parte superior del abdomen y tibia color castaño. Especies similares: Falco femoralis, Falco deiroleucus.",
		"url": "http://www.biodiversidad.co/fichas/234",
		"region": "Pacifico",
		"x": 548,
		"y": 332
	},
	{
		"nombre" : "geko salvaje",
		"shape" : "geko_salvaje.png",
		"image":"71510_orig.jpg",
		"descripcion": "A diferencia de H. brookii y de H. mabouia, esta especie habita principalmente en los árboles de bosque seco tropical, aunque a veces también se les puede encontrar en las paredes de las casas (Ayala y Castro Inédito).",
		"url": "http://www.biodiversidad.co/fichas/2809",
		"region": "Orinoquia",
		"x": 546,
		"y": 181
	},
	{
		"nombre" : "tortuga mordelona",
		"shape" : "tortuga_mordelona.png",
		"image":"13813_orig.jpg",
		"descripcion": "Son de hábitos acuáticos por lo que se les puede encontrar cerca a casi cualquier tipo de cuerpo de agua dulce con fondo blando y escondites disponibles, como por ejemplo troncos de árboles (Ernst et al. 2000).",
		"url": "http://www.biodiversidad.co/fichas/2756",
		"region": "Pacifico",
		"x": 462,
		"y": 470
	},
	{
		"nombre" : "chochos de bejuco",
		"shape" : "chochos_de_bejuco.png",
		"image":"08252_orig.jpg",
		"descripcion": "Esta especie se usa como insecticida macerando tallos y hojas. Las raíces se emplean en decocción como buen expectorante. Las semillas secas en maceración en agua fría se usan para combatir la conjuntivitis granulosa, orzuelos e irritaciones en los parpados. (García-Barriga 1992a).",
		"url": "http://www.biodiversidad.co/fichas/1764",
		"region": "Andina",
		"x": 430,
		"y": 252
	},
	{
		"nombre" : "helecho cilantrillo",
		"shape" : "helecho_cilantrillo.png",
		"image":"Adiantum_macrophyllum_foto_270x270.jpg",
		"descripcion": "Rizoma brevemente reptante, con escamas de un solo color, linear - lanceoladas, marrón a marrón oscuras, tallos de color púrpura a negro. Hojas agrupadas, de 30 a 65 cm de largo, arqueadas, una vez divididas (pinnadas), cada uno de los foliolos triangulares, lampiños en ambas superficies y un poco blancos en el envés.",
		"url": "http://www.biodiversidad.co/fichas/1172",
		"region": "Pacifico",
		"x": 530,
		"y": 236
	},
	{
		"nombre" : "totumo",
		"shape" : "totumo.png",
		"image":"Aegiphila_bogotensis_foto_270x270.jpg",
		"descripcion": "Se emplea como fuente de leña, cercas vivas, es utilizada como planta de sombrío en cafetales (Según Jardín Botánico José Celestino Mutis de Bogotá).",
		"url": "http://www.biodiversidad.co/fichas/2387",
		"region": "Caribe",
		"x": 322,
		"y": 290
	},
	{
		"nombre" : "campana dorada",
		"shape" : "campana_dorada.png",
		"image":"05297_orig.jpg",
		"descripcion": "Este bejuco trepador de abundante ramificación produce un látex abundante de color blanco al cortar su tallo. Sus hojas son simples y miden entre 6 y 12 cm de largo y entre 3 y 6 cm de ancho, son de color verde brillante, tienen borde entero y su textura es parecida a la del cuero (coriácea).",
		"url": "http://www.biodiversidad.co/fichas/1077",
		"region": "Andina",
		"x": 470,
		"y": 335
	},
	{
		"nombre" : "araucaria",
		"shape" : "araucaria.png",
		"image":"55982_orig.jpg",
		"descripcion": "Este árbol dioico alcanza 30 m de altura. Su copa es de forma piramidal. Su tronco es grueso, cilíndrico, muy recto y mide hasta 2,5 m de diámetro. Su corteza es suberosa (parecida al corcho), es muy gruesa y agrietada.",
		"url": "http://www.biodiversidad.co/fichas/1277",
		"region": "Andina",
		"x": 330,
		"y": 460
	},
	{
		"nombre" : "Bellísima",
		"shape" : "bellisima.png",
		"image":"58091_orig.jpg",
		"descripcion": "Habita en bosques secos tropicales, bosques húmedos tropicales, bosques húmedos y muy húmedos premontanos (Mahecha et al. 2004). Se propaga por estacas, con abundante luz solar y riego, de crecimiento rápido (Mahecha et al. 2004).",
		"url": "http://www.biodiversidad.co/fichas/1399",
		"region": "Caribe",
		"x": 279,
		"y": 392
	},
	{
		"nombre" : "Cachipay",
		"shape" : "cachipay.png",
		"image":"20684_orig.jpg",
		"descripcion": "El chontaduro es propio de regiones tropicales y prefiere zonas con alta precipitación pluvial y alta temperatura y suelos no inundables ni compactos. El medio más propicio son zonas cálidas con alta humedad.",
		"url": "http://www.biodiversidad.co/FICHAS/312",
		"region": "Pacifico",
		"x": 417,
		"y": 427
	},
	{
		"nombre" : "arbol orquidea",
		"shape" : "arbol_orquidea.png",
		"image":"93182_orig.jpg",
		"descripcion": "Según el Jardín Botánico Joaquín Antonio Uribe de Medellín es una especie melífera, maderable, que se emplea en la elaboración de mangos de herramientas y en la producción de leña. Se utiliza como sombrío, cerca viva, barrera rompevientos, en la protección y conservación de suelos, ya que es fijadora de nitrógeno; y como forraje. ",
		"url": "http://www.biodiversidad.co/FICHAS/1348",
		"region": "Andina",
		"x": 606,
		"y": 103
	},
	{
		"nombre" : "veranera",
		"shape" : "veranera.png",
		"image":"Bougainvillea_glabra_foto_270x270.jpg",
		"descripcion": "Arbusto trepador que crece solitario, alcanza hasta 6 m de altura, tallo reclinado y espinoso. Hojas lampiñas, verdes por ambas superficies, margen entera. Flores agrupadas, hojas que sostienen las flores de colores fucsia o moradas, las flores tubulares, amarillas.",
		"url": "http://www.biodiversidad.co/fichas/1148",
		"region": "Caribe",
		"x": 452,
		"y": 401
	},
	{
		"nombre" : "guayacán",
		"shape" : "guayacan.png",
		"image":"30988_orig.jpg",
		"descripcion": "Por un lado crece en bordes de carretera, caminos y ríos, o al interior de bosques secos o subxerofíticos y espinosos.",
		"url": "http://www.biodiversidad.co/fichas/272",
		"region": "Caribe",
		"x": 393,
		"y": 342
	},
	{
		"nombre" : "borrachero rojo",
		"shape" : "borrachero_rojo.png",
		"image":"31370_orig.jpg",
		"descripcion": "Crece en margen de pastizales, o en remanentes de bosques montanos (Gupta 1995); vive en bosques húmedos y muy húmedos montanos bajos y bosques secos montanos bajos (Mahecha et al. 2004).",
		"url": "http://www.biodiversidad.co/fichas/1238",
		"region": "Andina",
		"x": 385,
		"y": 356
	},
	{
		"nombre" : "cola de pescado",
		"shape" : "cola_de_pescado.png",
		"image":"46543_orig.jpg",
		"descripcion": "Esta palma presenta tallos agrupados de hasta 15 cm de diámetro. Sus hojas miden de 2 a 3 m de largo (eFloras 2008).",
		"url": "http://www.biodiversidad.co/fichas/1291",
		"region": "Andina",
		"x": 461,
		"y": 147
	},
	{
		"nombre" : "cola de pescado",
		"shape" : "cola_de_pescado.png",
		"image":"46543_orig.jpg",
		"descripcion": "Esta palma presenta tallos agrupados de hasta 15 cm de diámetro. Sus hojas miden de 2 a 3 m de largo (eFloras 2008).",
		"url": "http://www.biodiversidad.co/fichas/1291",
		"region": "Andina",
		"x": 461,
		"y": 147
	},
	{
		"nombre" : "ceiba",
		"shape" : "ceiba.png",
		"image":"06455_orig.jpg",
		"descripcion": "El volador puede crecer en orillas de ríos o tierra firme, usualmente comportándose como una especie de mediano porte de bosques medianos a bajos y asociados a diversas condiciones edáficas.",
		"url": "http://www.biodiversidad.co/fichas/274",
		"region": "Amazonas",
		"x": 320,
		"y": 294
	},
	{
		"nombre" : "Árbol nacional - Palma de cera",
		"shape" : "palma_de_cera.png",
		"image":"20973_orig.jpg",
		"descripcion": "Crece en zonas de bosque húmedo montano y montano bajo (Galeano y Bernal 2005). La mayor parte de su hábitat ha sido transformado en potreros por lo que la mayor concentración de individuos se encuentran en fragmentos de bosque. Se suelen encontrar también individuos aislados en medio de potreros (Vargas 2002).",
		"url": "http://www.biodiversidad.co/fichas/1086",
		"region": "Orinoquia",
		"x": 506,
		"y": 56
	},
	{
		"nombre" : "Café de Colombia",
		"shape" : "cafe.png",
		"image":"34765_orig.jpg",
		"descripcion": "Crece en zonas de bosque húmedo montano y montano bajo (Galeano y Bernal 2005). La mayor parte de su hábitat ha sido transformado en potreros por lo que la mayor concentración de individuos se encuentran en fragmentos de bosque. Se suelen encontrar también individuos aislados en medio de potreros (Vargas 2002).",
		"url": "http://www.biodiversidad.co/fichas/1232",
		"region": "Andina",
		"x": 492,
		"y": 252
	},
	{
		"nombre" : "Bala de cañón",
		"shape" : "bala_de_canon.png",
		"image":"25629_orig.jpg",
		"descripcion": "Su floración y fructificación es casi todo el año; los frutos maduran al cabo de 18 meses y no se desprenden del árbol hasta el año siguiente; la caída del follaje es desde mayo hasta julio, pierde parcialmente sus hojas y la renovación del follaje es desde agosto hasta septiembre (Mahecha et al. 2004).",
		"url": "http://www.biodiversidad.co/fichas/1008",
		"region": "Orinoquia",
		"x": 266,
		"y": 295
	},
	{
		"nombre" : "caucho",
		"shape" : "caucho.png",
		"image":"Ficus_benjamina_foto_270x270.jpg",
		"descripcion": "Es una planta maderable, con uso ornamental, alimento para la fauna silvestre en especial la avifauna. Además es utilizada como cerca viva, cortina rompevientos, para sombrío de cultivos y para controlar la erosión",
		"url": "http://www.biodiversidad.co/fichas/1054",
		"region": "Caribe",
		"x": 345,
		"y": 377
	},
	{
		"nombre" : "heliconia",
		"shape" : "heliconia.png",
		"image":"Heliconia_aemygdiana_foto_270x270.jpg",
		"descripcion": "Es una planta maderable, con uso ornamental, alimento para la fauna silvestre en especial la avifauna. Además es utilizada como cerca viva, cortina rompevientos, para sombrío de cultivos y para controlar la erosión",
		"url": "http://www.biodiversidad.co/fichas/957",
		"region": "Amazonas",
		"x": 677,
		"y": 80
	},
	{
		"nombre" : "platanilla",
		"shape" : "platanilla.png",
		"image":"Heliconia_aemygdiana_foto_270x270.jpg",
		"descripcion": "Es una especie con uso ornamental, se usa como flor de corte y es empleada en la decoración de jardines o macetas según el Jardín Botánico del Quindío (Calarcá).",
		"url": "http://www.biodiversidad.co/fichas/958",
		"region": "Andina",
		"x": 579,
		"y": 180
	},
	{
		"nombre" : "Árbol de las salchichas",
		"shape" : "arbol_de_las_salchichas.png",
		"image":"15372113393_eb90df1ac8.jpg",
		"descripcion": "Esta especie al igual que otras de su género es notable por sus flores y sus frutos dispuestos en largos péndulos.",
		"url": "http://www.biodiversidad.co/fichas/1488",
		"region": "Andina",
		"x": 590,
		"y": 314
	},
	{
		"nombre" : "Guayacán de Manizales",
		"shape" : "guayacan_de_manizales.png",
		"image":"52912_orig.jpg",
		"descripcion": "Puede crecer hasta 30 m de altura con un tronco de 1 m de diámetro. Con follaje glabro y de color rojizo. Las flores son amarillo-claras. Los frutos tienen forma elipsoide de 8 cm de longitud y 5 de ancho, con numerosas semillas aladas de color café y de 4 cm de longitud por 2 cm de ancho (Vargas 2002).",
		"url": "http://www.biodiversidad.co/fichas/1529",
		"region": "Andina",
		"x": 430,
		"y": 309
	},
	{
		"nombre" : "cinconegritos",
		"shape" : "cinconegritos.png",
		"image":"65221_orig.jpg",
		"descripcion": "Ornamental",
		"url": "http://www.biodiversidad.co/fichas/521",
		"region": "Andina",
		"x": 448,
		"y": 380
	},
	{
		"nombre" : "Flor nacional - Flor de mayo",
		"shape" : "flor_de_mayo.png",
		"image":"39064_orig.jpg",
		"descripcion": "Flores de a dos o tres por pedúnculo, de 18 cm de diámetro, muy variadas en color; pétalos y sépalos generalmente rosados, un pétalo modificado a manera de lengua amarillo en la garganta y en el borde púrpura crespo (Pérez-A. 1996).",
		"url": "http://www.biodiversidad.co/fichas/1380",
		"region": "Andina",
		"x": 390,
		"y": 488
	},
	{
		"nombre" : "malamadre",
		"shape" : "malamadre.png",
		"image":"06800_orig.jpg",
		"descripcion": "La planta mide de 40 a 60 cm de altura. Las hojas son laminares y brillantes. La inflorescencia es erecta, simple y un poco ramificada. Las flores son blancas con manchas amarillas (Souza et al. 2004).",
		"url": "http://www.biodiversidad.co/fichas/2076",
		"region": "Andina",
		"x": 447,
		"y": 488
	},
	{
		"nombre" : "cheflera",
		"shape" : "cheflera.png",
		"image":"07131_orig.jpg",
		"descripcion": "Este árbol presenta hojas de color verde oscuro, lisas y brillantes; tienen forma de mano y son enteras y rasgadas en la juventud (dimorfismo foliar). Esta especie prefiere suelos fértiles y bien drenados. Es fácil de cultivar pues retoña vigorosamente cuando se poda (Mahecha y Echeverri 1983).",
		"url": "http://www.biodiversidad.co/fichas/1274",
		"region": "Andina",
		"x": 352,
		"y": 511
	},
	{
		"nombre" : "Árbol de la vida", 
		"shape" : "arbol_de_la_vida.png",
		"image":"18477_orig.jpg",
		"descripcion": "Este árbol perenne alcanza hasta 15 metros de altura, su tronco de color pardo, curvo, resinoso y escamoso alcanza hasta 60 cm de diámetro. Su copa es amplia y de forma aplanada, su follaje es de color verde claro y su ramificación es muy abundante.",
		"url": "http://www.biodiversidad.co/fichas/1073",
		"region": "Andina",
		"x": 533,
		"y": 280
	},
	{
		"nombre" : "Acacio santandereano", 
		"shape" : "acacio_santandereano.png",
		"image":"96104_orig.jpg",
		"descripcion": "Árbol de hasta 8 m de alto, con 6 m de envergadura. Pecíolos afelpados y hasta 12 pares de hojuelas ovales. Hojas compuestas, con foliolos pareados. Las flores son amarillas y están dispuestas en densas cabezuelas. Frutos en vainas, largas, aplastadas o redondeadas (Burnie et al. 2006).",
		"url": "http://www.biodiversidad.co/fichas/2339",
		"region": "Caribe",
		"x": 519,
		"y": 331
	},
	{
		"nombre" : "Amor de madre", 
		"shape" : "amor_de_madre.png",
		"image":"94869_orig.jpg",
		"descripcion": "Bosque seco tropical, bosque húmedo tropical, bosque húmedo premontano y bosque muy húmedo premontano (Mahecha et al. 2004).",
		"url": "http://www.biodiversidad.co/fichas/2339",
		"region": "Pacifico",
		"x": 505,
		"y": 147
	},
	{
		"nombre" : "Cactus", 
		"shape" : "cactus.png",
		"image":"58453_orig.jpg",
		"descripcion": "Según el Jardín Botánico Joaquín Antonio Uribe de Medellín es una especie ornamental. Sus frutos se consumen frescos o se usan para hacer bebidas refrescantes y mermeladas. Esta planta también se utiliza como cerca viva (Romero 1991).",
		"url": "http://www.biodiversidad.co/fichas/1707",
		"region": "Andina",
		"x": 536,
		"y": 96
	},
	{
		"nombre" : "orquidea", 
		"shape" : "orquidea.png",
		"image":"Cattleya_schroederae_foto_270x270.jpg",
		"descripcion": "Flores muy grandes, con pétalos y sépalos encartuchados, garganta anaranjada y flor muy perfumada (Pérez-A. 1978).",
		"url": "http://www.biodiversidad.co/fichas/1378",
		"region": "Orinoquia",
		"x": 626,
		"y": 163
	},
	{
		"nombre" : "Amapola", 
		"shape" : "amapola.png",
		"image":"65826_orig.jpg",
		"descripcion": "Esta hierba anual es erecta y alcanza hasta un metro de altura aproximadamente. No tiene vellos (es glabra) y es fragante. Sus hojas miden hasta 6 cm de largo y son opuestas imparipinnadas (compuestas de hojuelas insertas a uno y otro lado en un número impar).",
		"url": "http://www.biodiversidad.co/fichas/1109",
		"region": "Amazonas",
		"x": 612,
		"y": 123
	},
	{
		"nombre" : "caulote", 
		"shape" : "caulote.png",
		"image":"46942_orig.jpg",
		"descripcion": "Habita bosques secos tropicales, bosques húmedos tropicales y bosques húmedos y muy húmedos premontanos (Mahecha et al. 2004). Crece en el borde de bosques de galería (Acero 2005).",
		"url": "http://www.biodiversidad.co/fichas/1243",
		"region": "Caribe",
		"x": 593,
		"y": 156
	},
	{
		"nombre" : "bruja", 
		"shape" : "bruja.png",
		"image":"58518_orig.jpg",
		"descripcion": "Es una hierba perenne suculenta de hojas arrocetadas u opuestas simples que crece hasta un metro de altura. Sus inflorescencias crecen en la parte terminal del tallo y sus flores son de color rojizo (Bernal y Correa 1990 y Vargas 2002).",
		"url": "http://www.biodiversidad.co/fichas/1143",
		"region": "Pacifico",
		"x": 407,
		"y": 473
	}







	









]