var App = angular.module("Diversidad", ['ngRoute', 'ngDialog', 'mm.foundation']);

App.config(['$routeProvider', function($routeProvider){
	$routeProvider
		.when('/', {
			templateUrl: 'views/mapa.html',
			controller: 'MapaController',
			replace: true
		})

		.when('/region', {
			templateUrl: 'views/region.html',
			controller: 'RegionController',
			replace: true
		})

		.otherwise({
			redirectTo: '/'
		})
}]);

App.directive('svgMap', ['$compile', function($compile){
	return{
		restrict: 'A',
		templateUrl: 'img/mapa_regiones.svg',
		link: function(scope, elm, attr){
			var regiones = elm[0].querySelectorAll("g");
			angular.forEach(regiones, function(path, key){
				var regionElement = angular.element(path);
				regionElement.attr("region", "");
				$compile(regionElement)(scope);
			})
		}
	}
}])

.directive('region', ['$compile', '$location', function($compile, $location){
	return{
		restrict: 'A',
		scope: true,
		link: function(scope, elm, attr){
			scope.elementId = elm.attr("id");
			
			scope.regionClick = function(){
				$location.path('region').search({r:scope.elementId});
			}

			scope.enterTooltip = function(){
				$('.tooltip_m').removeClass("open");
				$('.tooltip_m.' + scope.elementId).addClass("open");
			}

			scope.leaveTooltip = function(){
				$('.tooltip').removeClass("open");
			}

			elm.attr("ng-click", "regionClick()");
			elm.attr("ng-mouseenter", "enterTooltip()");
			elm.attr("ng-mouseleave", "leaveTooltip()");

			elm.removeAttr("region");
			$compile(elm)(scope);
		}
	}
}])

.directive('svgRegion', [function(){
	return{
		restrict: 'A',
		link: function(scope, elm, attr){
			scope.getRegionUrl = function(){
				return 'img/' + scope.region+'.svg'
			}
		},

		template: '<div ng-include="getRegionUrl()"></div>'
	}
}]);