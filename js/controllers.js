App.controller('MapaController', ['$scope', function($scope){
	$('.content').removeClass('green');
	$scope.title = "Regiones de Colombia";
}]);

App.controller('RegionController', ['$scope', '$location','Animales', 'ngDialog', function($scope, $location , Animales, ngDialog){
	$scope.region = $location.search().r;

	$('.content').addClass('green');
	$scope.title = "Region " + $scope.region;
	$scope.animales = Animales.getAnimalByRegion($scope.region);

	$scope.openInfo = function(e, url){
		e.preventDefault();
		ngDialog.open({
		    template: '<iframe src="'+url+'" frameborder="0" width="100%" height="400px"></iframe>',
		    plain: true
		});
	}
	
}]);